# ---------------------------------------------------------------------------
# - SEC0020.als                                                             -
# - afnix:sec module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2020 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   gcm mode test unit
# @author amaury darsch

# get the module
interp:library "afnix-sec"

# 128 bits key buffer
trans kbuf "00000000000000000000000000000000"
# 12 bytes initial vector "000000000000000000000000"
trans iv "000000000000000000000000"

# 16 bytes input vector 00000000000000000000000000000000
trans ivec (Vector
  0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00
  0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00
)

# create a key with a buffer
trans key (afnix:sec:Key kbuf)
assert 128 (key:get-bits)

# create the cipher
trans aes (afnix:sec:Aes key)
aes:set-block-mode afnix:sec:ModeCipher:MODE-ECB
aes:set-padding-mode afnix:sec:ModeCipher:PAD-NONE

# create the gcm by cipher
trans gcm (afnix:sec:Gcm aes)
assert true (afnix:sec:aead-cipher-p gcm)
assert true (afnix:sec:gcm-p gcm)
assert "Gcm" (gcm:repr)
# set the initial vector
gcm:set-iv iv

# process a buffer
trans ib (Buffer ivec)
trans ob (Buffer)
assert 16 (gcm:stream ob ib)#
assert "0388DACE60B6A392F328C2B971B2FE78" (ob:format)
# check the tag
trans tb (gcm:get-authentication-tag)
assert "AB6E47D42CEC13BDF53A67B21257BDDF" (tb:format)

# reverse the cipher and check
gcm:set-reverse true
assert true (gcm:get-reverse)

trans db (Buffer)
assert 16 (gcm:stream db ob)
# check decode
trans ib (Buffer ivec)
assert (ib:format) (db:format)
# check the tag
trans bt (gcm:get-authentication-tag)
assert (tb:format) (bt:format)

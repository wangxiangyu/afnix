# ---------------------------------------------------------------------------
# - SEC0200.als                                                             -
# - afnix:sec module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2020 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   galois field test unit
# @author amaury darsch

# get the module
interp:library "afnix-sec"

# create a rinjdael galois field
trans gf (afnix:sec:Galois 0x11BR)
assert true (afnix:sec:galois-p gf)
assert "Galois" (gf:repr)

# check modulo
assert 1R (gf:mod 0x3F7ER)
# check addition
assert 0x99R (gf:add 0x53R 0xCAR)
# check multiplication
assert 0x1R (gf:mul 0x53R 0xCAR)

# check gcm multiplication
trans p 0xE1000000000000000000000000000000R
trans x 0x0388DACE60B6A392F328C2B971B2FE78R
trans y 0x66E94BD4EF8A2C3B884CFA59CA342B2ER

trans gf (afnix:sec:Galois p)
assert 0x5E2EC746917062882C85B0685353DEB7R (gf:gmm x y)

// ---------------------------------------------------------------------------
// - Ecc.cpp                                                                 -
// - afnix:sec module - elliptic curve cryptography class implementation     -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2020 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Ecc.hpp"
#include "Ecc.hxx"
#include "Vector.hpp"
#include "Boolean.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 2;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the ecc supported quarks
  static const long QUARK_ADD    = zone.intern ("add");
  static const long QUARK_VALIDP = zone.intern ("valid-p");
  
  // return true if the given quark is defined

  bool Ecc::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Object::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // apply this object with a set of arguments and a quark
  
  Object* Ecc::apply (Evaluable* zobj, Nameset* nset, const long quark,
		      Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();
    
    // check for 0 argument
    if (argc == 0) {
      if (quark == QUARK_VALIDP) return new Boolean (valid ());
    }
    // check for 1 argument
    if (argc == 1) {
      if (quark == QUARK_VALIDP) {
	Ecp p = ecc_to_ecp (argv->get (0));
	return new Boolean (valid (p));
      }
    }
    // check for 2 arguments
    if (argc == 2) {
      if (quark == QUARK_ADD) {
	Ecp px = ecc_to_ecp (argv->get (0));
	Ecp py = ecc_to_ecp (argv->get (1));
	return new Ecp (add (px, py));
      }
    }
    // call the object method
    return Object::apply (zobj, nset, quark, argv);
  }
}
